package club.casadeballoon.balloontracker

import android.content.Context
import android.hardware.Camera
import android.hardware.SensorEvent
import android.location.Location
import android.preference.PreferenceManager
import android.util.Log
import java.util.*
import java.util.concurrent.ConcurrentHashMap

object Status {

    @Volatile
    var lastLocation : Location? = null;
    val lastSensorEventMap = ConcurrentHashMap<String, SensorEvent>();
    @Volatile
    var camera: Camera? = null;


    @Volatile
    var smsTimerLastAttempt : Date? = null;
    @Volatile
    var smsTimerLastSuccess : Date? = null;
    @Volatile
    var smsTimerAttemptCount : Int = 0;
    @Volatile
    var smsTimerSuccessCount : Int = 0;

    @Volatile
    var trackerTimerLastAttempt : Date? = null;
    @Volatile
    var trackerTimerLastSuccess : Date? = null;
    @Volatile
    var trackerTimerAttemptCount : Int = 0;
    @Volatile
    var trackerTimerSuccessCount : Int = 0;

    @Volatile
    var photoTimerLastAttempt : Date? = null;
    @Volatile
    var photoTimerLastSuccess : Date? = null;
    @Volatile
    var photoTimerAttemptCount : Int = 0;
    @Volatile
    var photoTimerSuccessCount : Int = 0;

    @Volatile
    var sensorTimerLastAttempt : Date? = null;
    @Volatile
    var sensorTimerLastSuccess : Date? = null;
    @Volatile
    var sensorTimerAttemptCount : Int = 0;
    @Volatile
    var sensorTimerSuccessCount : Int = 0;


    fun getRunning(context: Context): Boolean {
        return PreferenceManager
                .getDefaultSharedPreferences(context.applicationContext)
                .getBoolean(Constants.CONFIG_RUNNING_KEY, false);
    }

    fun setRunning(context: Context, running: Boolean) {
        val settings = PreferenceManager.getDefaultSharedPreferences(context.applicationContext);
        val editor = settings.edit();
        editor.putBoolean(Constants.CONFIG_RUNNING_KEY, running);
        editor.commit();
        Log.d(Constants.DEBUG_TAG, "Running status saved to preferences");
    }




}