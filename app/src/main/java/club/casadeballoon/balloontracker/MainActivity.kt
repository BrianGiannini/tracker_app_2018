package club.casadeballoon.balloontracker

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Switch
import android.widget.TextView
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.method.ScrollingMovementMethod
import android.widget.Toast
import java.io.File
import java.util.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import android.app.admin.DevicePolicyManager
import android.content.ComponentName


class MainActivity : AppCompatActivity() {

    var confirm1: Boolean = false;
    var confirm2: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val statusText = findViewById<TextView>(R.id.statusText);
        statusText.movementMethod = ScrollingMovementMethod();
        Config.readFromPreferences(this);
    }

    override fun onResume() {
        super.onResume();
        refresh();
    }

    fun refresh() {
        val statusBuilder = StringBuilder();
        val statusDateFormat = SimpleDateFormat("HH:mm:ss");

        statusBuilder.appendln("Name: "+Config.name);

        val currentTime = statusDateFormat.format(Date());
        statusBuilder.appendln("Refreshed: "+currentTime);

        val isServiceRunning = if(Status.getRunning(this)) "Running" else "Stopped";
        statusBuilder.appendln("Service: "+isServiceRunning);

        val location = Status.lastLocation;
        val posFormat = DecimalFormat("#.####");
        val altFormat = DecimalFormat("#.#");
        val locationSummary = "("+if(location?.latitude != null) {
            posFormat.format(location.latitude)
        } else {
            "???"
        }+","+if(location?.longitude != null) {
            posFormat.format(location.longitude)
        } else {
            "???"
        }+") +"+if(location?.altitude != null) {
            altFormat.format(location.altitude)
        } else {
            "???"
        }+"m @"+if(location?.time != null) {
            statusDateFormat.format(Date(location.time))
        } else {
            "???"
        }
        statusBuilder.appendln("Location: "+locationSummary);

        val smsSummary = Status.smsTimerSuccessCount.toString()+" ("+if(Status.smsTimerLastSuccess != null) {
            statusDateFormat.format(Status.smsTimerLastSuccess)
        } else {
            "???"
        }+") of "+Status.smsTimerAttemptCount.toString()+" ("+if(Status.smsTimerLastAttempt != null) {
            statusDateFormat.format(Status.smsTimerLastAttempt)
        } else {
            "???"
        }+")";
        statusBuilder.appendln();
        statusBuilder.appendln("SMS Phone: "+Config.smsPhoneNumber);
        statusBuilder.appendln("SMS Status: "+smsSummary);

        val trackerSummary = Status.trackerTimerSuccessCount.toString()+" ("+if(Status.trackerTimerLastSuccess != null) {
            statusDateFormat.format(Status.trackerTimerLastSuccess)
        } else {
            "???"
        }+") of "+Status.trackerTimerAttemptCount.toString()+" ("+if(Status.trackerTimerLastAttempt != null) {
            statusDateFormat.format(Status.trackerTimerLastAttempt)
        } else {
            "???"
        }+")";
        statusBuilder.appendln();
        statusBuilder.appendln("Tracker URL: "+Config.trackerBaseUrl);
        statusBuilder.appendln("Tracker Status: "+trackerSummary);

        val sensorSummary = Status.sensorTimerSuccessCount.toString()+" ("+if(Status.sensorTimerLastSuccess != null) {
            statusDateFormat.format(Status.sensorTimerLastSuccess)
        } else {
            "???"
        }+") of "+Status.sensorTimerAttemptCount.toString()+" ("+if(Status.sensorTimerLastAttempt != null) {
            statusDateFormat.format(Status.sensorTimerLastAttempt)
        } else {
            "???"
        }+")";
        statusBuilder.appendln();
        statusBuilder.appendln("Sensors Found: "+Status.lastSensorEventMap.values.map { sensorEvent -> sensorEvent.sensor.name }.joinToString(", "));
        statusBuilder.appendln("Sensors Status: "+sensorSummary);

        val photoSummary = Status.photoTimerSuccessCount.toString()+" ("+if(Status.photoTimerLastSuccess != null) {
            statusDateFormat.format(Status.photoTimerLastSuccess)
        } else {
            "???"
        }+") of "+Status.photoTimerAttemptCount.toString()+" ("+if(Status.photoTimerLastAttempt != null) {
            statusDateFormat.format(Status.photoTimerLastAttempt)
        } else {
            "???"
        }+")";
        statusBuilder.appendln();
        statusBuilder.appendln("Photo Status: "+photoSummary);

        val statusText = findViewById<TextView>(R.id.statusText);
        statusText.text = statusBuilder.toString();

    }

    fun onStartClick(v: View) {
        if(checkAndResetToggles()) {
            Log.d(Constants.DEBUG_TAG, "Begin service start");
            Status.setRunning(this, true);

            val intent = Intent(this, MainService::class.java);
            intent.setAction(Constants.ACTION_START_FOREGROUND_SERVICE);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }
        }
    }

    fun onInitClick(v: View) {
        if(checkAndResetToggles()) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                            android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.WAKE_LOCK,
                            android.Manifest.permission.RECEIVE_BOOT_COMPLETED,
                            android.Manifest.permission.SEND_SMS,
                            android.Manifest.permission.INTERNET
                    ), 0);

            GPSMod.patchGPS(this);

            val dpmReceiver = ComponentName(this, ScreenLockReceiver::class.java);
            val dpm = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager;
            if(!dpm.isAdminActive(dpmReceiver)) {
                val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, dpmReceiver);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Enable device admin");
                startActivity(intent);
            }

            Toast.makeText(this, "Init Success!", Toast.LENGTH_SHORT).show();
        }
    }

    fun onConfigClick(v: View) {
        if(checkAndResetToggles()) {
            Log.d(Constants.DEBUG_TAG, "Launching config");
            val intent = Intent(this, ConfigActivity::class.java);
            startActivity(intent);
        }
    }

    fun onStopClick(v: View) {
        if(checkAndResetToggles()) {
            Log.d(Constants.DEBUG_TAG, "Begin service stop");
            Status.setRunning(this, false);

            val intent = Intent(this, MainService::class.java);
            intent.setAction(Constants.ACTION_STOP_FOREGROUND_SERVICE);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }
        }
    }

    fun onLocationClick(v: View) {
        if(checkAndResetToggles()) {
            Log.d(Constants.DEBUG_TAG, "Testing location update");

            val locationManager = (getSystemService(Context.LOCATION_SERVICE) as LocationManager);
            if(ContextCompat.checkSelfPermission(applicationContext, android.Manifest.permission.ACCESS_FINE_LOCATION).equals(PackageManager.PERMISSION_GRANTED)) {
                val location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                try {
                    Utils.sendLocationSMS(location);
                } catch (e: Exception) {
                    Log.e(Constants.DEBUG_TAG, "Error sending location SMS", e);
                    Toast.makeText(this, "SMS Error: "+e.message, Toast.LENGTH_SHORT).show();
                }
                try {
                    Utils.sendTrackerPing(location);
                } catch (e: Exception) {
                    Log.e(Constants.DEBUG_TAG, "Error pinging location tracker", e);
                    Toast.makeText(this, "Tracker Error: "+e.message, Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    fun onPhotoClick(v: View) {
        if(checkAndResetToggles()) {
            Log.d(Constants.DEBUG_TAG, "Testing photo");
            val runPrefix = Config.name+"_"+SimpleDateFormat("yyyyMMdd_HHmmss").format(Date());

            val storageRoot = Utils.getStorageRoot(this);
            val exposures = Constants.DEFAULT_EXPOSURES.map { exposure ->
                Pair(File(storageRoot, runPrefix+"_0001_"+(exposure+50).toString()+"_"+Constants.PHOTO_FILE), exposure);
            }

            val locationManager = (getSystemService(Context.LOCATION_SERVICE) as LocationManager);
            var location: Location? = null;
            if(ContextCompat.checkSelfPermission(applicationContext, android.Manifest.permission.ACCESS_FINE_LOCATION).equals(PackageManager.PERMISSION_GRANTED)) {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }

            Utils.openCamera();

            Utils.takePhotoBurst(this, location, exposures, { isSuccess ->
                if(isSuccess && exposures.map({ p -> p.first.exists()}).reduce({a,b -> a && b})) {
                    Toast.makeText(this, "Photo Burst Success", Toast.LENGTH_SHORT).show();
                }
                Utils.closeCamera();
            });
        }

    }

    fun onRefreshClick(v: View) {
        refresh();
    }

    fun checkAndResetToggles() : Boolean {
        val confirmToggle1 = findViewById<Switch>(R.id.confirmToggle1);
        val confirmToggle2 = findViewById<Switch>(R.id.confirmToggle2);
        val isConfirmed = confirmToggle1.isChecked && confirmToggle2.isChecked;
        confirmToggle1.isChecked = false;
        confirmToggle2.isChecked = false;
        val confirmText = findViewById<TextView>(R.id.confirmText);
        if(isConfirmed) {
            confirmText.text = "Is Confirmed";
        } else {
            confirmText.text = "Not Confirmed";
        }
        return isConfirmed;
    }

}
