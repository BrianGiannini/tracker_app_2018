package club.casadeballoon.balloontracker

import android.content.Context
import android.util.Log
import org.json.JSONObject
import android.preference.PreferenceManager


object Config {

    @Volatile
    var name: String = "ToBeNamed";

    @Volatile
    var smsIntervalSeconds: Int = 60;
    @Volatile
    var smsPhoneNumber: String = "555SMS5555";

    @Volatile
    var trackerIntervalSeconds: Int = 20;
    @Volatile
    var trackerBaseUrl: String = "http://tracker.url/endpoint?param=true";

    @Volatile
    var photoIntervalSeconds: Int = 30;
    @Volatile
    var photoUseSingleShot: Boolean = true;

    @Volatile
    var sensorIntervalSeconds: Int = 10;

    fun serializeToString() : String {
        val obj = JSONObject();
        obj.put("name", name);
        obj.put("smsIntervalSeconds", smsIntervalSeconds);
        obj.put("smsPhoneNumber", smsPhoneNumber);
        obj.put("trackerIntervalSeconds", trackerIntervalSeconds);
        obj.put("trackerBaseUrl", trackerBaseUrl);
        obj.put("photoIntervalSeconds", photoIntervalSeconds);
        obj.put("photoUseSingleShot", photoUseSingleShot);
        obj.put("sensorIntervalSeconds", sensorIntervalSeconds);
        return obj.toString(2);
    }

    fun readFromString(json: String) {
        try {
            val obj = JSONObject(json);
            name = obj.optString("name", name);
            smsIntervalSeconds = obj.optInt("smsIntervalSeconds", smsIntervalSeconds);
            smsPhoneNumber = obj.optString("smsPhoneNumber", smsPhoneNumber);
            trackerIntervalSeconds = obj.optInt("trackerIntervalSeconds", trackerIntervalSeconds);
            trackerBaseUrl = obj.optString("trackerBaseUrl", trackerBaseUrl);
            photoIntervalSeconds = obj.optInt("photoIntervalSeconds", photoIntervalSeconds);
            photoUseSingleShot = obj.optBoolean("photoUseSingleShot", photoUseSingleShot);
            sensorIntervalSeconds = obj.optInt("sensorIntervalSeconds", sensorIntervalSeconds);
        } catch (e: Exception) {
            Log.e(Constants.DEBUG_TAG, "Error reading config JSON: \n"+json, e);
        }
    }

    fun readFromPreferences(context: Context) {
        val json = PreferenceManager
                .getDefaultSharedPreferences(context.applicationContext)
                .getString(Constants.CONFIG_PREFERENCES_KEY, "{}")
                .trim();
        if(json.startsWith("{") && json.endsWith("}")) {
            readFromString(json);
            Log.d(Constants.DEBUG_TAG, "Config loaded from preferences");
        }
    }

    fun saveToPreferences(context: Context) {
        val settings = PreferenceManager.getDefaultSharedPreferences(context.applicationContext);
        val editor = settings.edit();
        editor.putString(Constants.CONFIG_PREFERENCES_KEY, serializeToString());
        editor.commit();
        Log.d(Constants.DEBUG_TAG, "Config saved to preferences");
    }

}