package club.casadeballoon.balloontracker

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import java.io.File

class StartupReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        // Kill GC100 default camera app on startup
        try {
            var proc: java.lang.Process = Runtime.getRuntime().exec(
                    arrayOf("su", "-c", "am start -c android.intent.category.HOME -a android.intent.action.MAIN"))
            proc.waitFor()
            proc = Runtime.getRuntime().exec(
                    arrayOf("su", "-c", "killall com.sec.android.app.camera"))
            proc.waitFor()
        } catch (ex: Throwable) {
            Log.d(Constants.DEBUG_TAG, "Error killind GC100 camera app", ex);
        }

        if(Status.getRunning(context)) {
            try {
                val logFile = File(Utils.getStorageRoot(context), Constants.LOG_FILE);
                Utils.logToFile(logFile, "Restarting servince on boot");
            } catch (ex: Exception) {
                Log.d(Constants.DEBUG_TAG, "Error logging service start on boot", ex);
            }
            val intent = Intent(context, MainService::class.java);
            intent.setAction(Constants.ACTION_START_FOREGROUND_SERVICE);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            } else {
                context.startService(intent);
            }
        }
    }

}
