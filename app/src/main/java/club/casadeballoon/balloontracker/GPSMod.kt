package club.casadeballoon.balloontracker

import android.content.Context
import android.util.Log
import android.widget.Toast
import java.io.File
import java.security.MessageDigest

// cmp -l *.original *.modified | gawk '{printf "%08X %02X %02X\n", $1-1, strtonum(0$2), strtonum(0$3)}'
data class GPSDInfo(
        val name: String,
        val originalMD5: String,
        val modifiedMD5: String,
        val addresses: List<Int>,
        val originalBytes: List<Byte>,
        val modifiedBytes: List<Byte>
)


object GPSMod {
    fun patchGPS(context: Context) {
        val gpsdInfos = listOf(
                // Samsung Galaxy S4 i9500 cm-11-20151003-UNOFFICIAL-GearCM-i9500
                GPSDInfo(
                        name = "gpsd",
                        originalMD5 = "0bb74bc0f03872888bc8165cc94e5a17",
                        modifiedMD5 = "6244e7fe1e1bbc6c5727cf86157e3ee9",
                        addresses = listOf(0x00191AB6, 0x00191AB7, 0x00191AB8, 0x00191AB9),
                        originalBytes = listOf(0xF3, 0xF7, 0xA3, 0xF8).map(Int::toByte),
                        modifiedBytes = listOf(0x00, 0xBF, 0x00, 0xBF).map(Int::toByte)
                ),
                // Samsung Galaxy Camera GC100 Stock 4.1.2
                GPSDInfo(
                        name = "gpsd_4752",
                        originalMD5 = "ef972feb56b443f8cae79ce95d9f3587",
                        modifiedMD5 = "abe5b4e12c3ffab010b09a6c92a8cc7e",
                        addresses = listOf(0x000E7914, 0x000E7915, 0x000E7916, 0x000E7917),
                        originalBytes = listOf(0xEF, 0xF7, 0x34, 0xFC).map(Int::toByte),
                        modifiedBytes = listOf(0x00, 0xBF, 0x00, 0xBF).map(Int::toByte)
                ),
                // Samsung Galaxy Zoom 2 C115L Stock 4.4.2
                GPSDInfo(
                        name = "gpsd",
                        originalMD5 = "2c06261f773cbc017dacb3f5b5f9cfde",
                        modifiedMD5 = "e7f5429b5aea14460ed2f4882ee320e0",
                        addresses = listOf(0x0018EA96, 0x0018EA97, 0x0018EA98, 0x0018EA99),
                        originalBytes = listOf(0xF5, 0xF7, 0x2F, 0xFD).map(Int::toByte),
                        modifiedBytes = listOf(0x00, 0xBF, 0x00, 0xBF).map(Int::toByte)
                )
        );

        val storageRoot = Utils.getStorageRoot(context)!!;
        gpsdInfos.forEach({ gpsdInfo ->
            val currentFilePath = "/system/bin/"+gpsdInfo.name;

            val currentGpsdFile = File(currentFilePath);
            if(currentGpsdFile.exists()) {
                val allBytes = currentGpsdFile.readBytes();
                val currentGpsdMD5 = MessageDigest.getInstance("MD5")
                        .digest(allBytes)
                        .map { String.format("%02x", it) }
                        .joinToString(separator = "");
                if(currentGpsdMD5 == gpsdInfo.originalMD5) {

                    // Save backup file
                    val originalBackupFile = File(storageRoot, gpsdInfo.name+".original");
                    originalBackupFile.writeBytes(allBytes);
                    Log.d(Constants.DEBUG_TAG, "Found original "+gpsdInfo.name+" ("+allBytes.size.toString()+") MD5: "+currentGpsdMD5);

                    // Patch original
                    val modifiedBytes = allBytes.clone();
                    for((index, address) in gpsdInfo.addresses.withIndex()) {
                        modifiedBytes[address] = gpsdInfo.modifiedBytes[index];
                    }
                    val modifiedGpsdMD5 = MessageDigest.getInstance("MD5")
                            .digest(modifiedBytes)
                            .map { String.format("%02x", it) }
                            .joinToString(separator = "");
                    if(modifiedGpsdMD5 == gpsdInfo.modifiedMD5) {
                        val modifiedBackupFile = File(storageRoot, gpsdInfo.name+".modified");
                        modifiedBackupFile.writeBytes(modifiedBytes);
                        val commands = listOf(
                                "mount -o remount,rw /system",
                                "rm /system/bin/"+gpsdInfo.name,
                                "cat "+modifiedBackupFile.absolutePath+" > /system/bin/"+gpsdInfo.name,
                                "chmod 777 /system/bin/"+gpsdInfo.name
                        )
                        try {
                            commands.forEach({command ->
                                Log.d(Constants.DEBUG_TAG, "Running as root: "+command);
                                Runtime.getRuntime().exec(arrayOf("su", "-c", command)).waitFor();
                            });
                            Log.d(Constants.DEBUG_TAG, "Replaced "+gpsdInfo.name+" with modified version");
                        } catch (e: Exception) {
                            Log.e(Constants.DEBUG_TAG, "Error replacing patched "+gpsdInfo.name, e);
                        }
                    }
                }
            }

            if(currentGpsdFile.exists()) {
                val allBytes = currentGpsdFile.readBytes();
                val currentGpsdMD5 = MessageDigest.getInstance("MD5")
                        .digest(allBytes)
                        .map { String.format("%02x", it) }
                        .joinToString(separator = "");
                if(currentGpsdMD5 == gpsdInfo.modifiedMD5) {
                    Log.d(Constants.DEBUG_TAG, "Found modified "+gpsdInfo.name+" ("+allBytes.size.toString()+") MD5: "+currentGpsdMD5);
                    Toast.makeText(context, "Modified "+gpsdInfo.name+" found!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

}
