package club.casadeballoon.balloontracker

import android.app.Notification
import android.app.Service
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.location.LocationManager
import android.util.Log
import android.widget.Toast
import java.io.File
import android.location.GpsStatus.NmeaListener
import android.support.v4.content.ContextCompat
import android.location.Location
import android.location.LocationListener
import android.hardware.SensorManager
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import java.util.*
import android.content.IntentFilter
import android.os.*
import android.support.v4.app.NotificationCompat
import java.text.SimpleDateFormat


class MainService : Service() {

    // Power management
    @Volatile
    var powerManager: PowerManager? = null;
    @Volatile
    var wakeLock: PowerManager.WakeLock? = null;
    @Volatile
    var photoWakeLock: PowerManager.WakeLock? = null;

    // Storage
    @Volatile
    var runPrefix: String? = null;
    @Volatile
    var storageRoot: File? = null;
    @Volatile
    var logFile: File? = null;
    @Volatile
    var nmeaLogFile: File? = null;
    @Volatile
    var sensorLogFile: File? = null;

    // Location
    @Volatile
    var locationManager: LocationManager? = null;
    inner class NMEALogListener : NmeaListener {
        override fun onNmeaReceived(timestamp: Long, nmea: String) {
            Utils.appendToFile(nmeaLogFile, nmea)
        }
    }
    inner class NoopLocationListener() : LocationListener {
        override fun onLocationChanged(location: Location) {
            Status.lastLocation = location;
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }
    @Volatile
    var nmeaLocationListener: NMEALogListener? = null
    @Volatile
    var noopLocationListener: NoopLocationListener? = null

    // Sensors
    @Volatile
    var sensorManager: SensorManager? = null
    @Volatile
    var pressureSensor: Sensor? = null
    @Volatile
    var temperatureSensor: Sensor? = null
    @Volatile
    var humiditySensor: Sensor? = null
    inner class NoopSensorEventListener : SensorEventListener {
        override fun onSensorChanged(sensorEvent: SensorEvent) {
            Status.lastSensorEventMap.put(sensorEvent.sensor.name, sensorEvent);
        }
        override fun onAccuracyChanged(sensor: Sensor, i: Int) {}
    }
    @Volatile
    var sensorEventListener: NoopSensorEventListener? = null


    // Timers
    inner class SMSTimerTask() : TimerTask() {
        override fun run() {
            Status.smsTimerLastAttempt = Date();
            Status.smsTimerAttemptCount = Status.smsTimerAttemptCount + 1;
            try {
                val location = getLastKnownLocation();
                Utils.sendLocationSMS(location);
                Status.smsTimerLastSuccess = Date();
                Status.smsTimerSuccessCount = Status.smsTimerSuccessCount + 1;
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error sending SMS to "+Config.smsPhoneNumber, e);
            }
        }
    }
    @Volatile
    var smsTimer: Timer? = null

    inner class TrackerTimerTask() : TimerTask() {
        override fun run() {
            Status.trackerTimerLastAttempt = Date();
            Status.trackerTimerAttemptCount = Status.trackerTimerAttemptCount + 1;
            try {
                val location = getLastKnownLocation();
                Utils.sendTrackerPing(location);
                Status.trackerTimerLastSuccess = Date();
                Status.trackerTimerSuccessCount = Status.trackerTimerSuccessCount + 1;
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error pinging tracker at "+Config.trackerBaseUrl, e);
            }
        }
    }
    @Volatile
    var trackerTimer: Timer? = null

    inner class SensorTimerTask() : TimerTask() {
        override fun run() {
            Status.sensorTimerLastAttempt = Date();
            Status.sensorTimerAttemptCount = Status.sensorTimerAttemptCount + 1;
            try {
                val batteryIntent = applicationContext.registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED));
                val batteryMillivolts = batteryIntent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);
                val location = getLastKnownLocation();
                val sensorEvents = Status.lastSensorEventMap.values.toList();
                Utils.logSensorData(sensorLogFile, sensorEvents, location, batteryMillivolts);
                Status.sensorTimerLastSuccess = Date();
                Status.sensorTimerSuccessCount = Status.sensorTimerSuccessCount + 1;
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error logging sensors to "+(sensorLogFile?.canonicalPath ?: "/unknown"), e);
            }
        }
    }
    @Volatile
    var sensorTimer: Timer? = null

    inner class PhotoTimerTask() : TimerTask() {
        override fun run() {
            Status.photoTimerLastAttempt = Date();
            Status.photoTimerAttemptCount = Status.photoTimerAttemptCount + 1;
            try {
                Log.d(Constants.DEBUG_TAG, "Attempting to take photo");

                if(photoWakeLock != null) {
                    try {
                        photoWakeLock?.release();
                    } catch (e: Exception) {
                        Log.e(Constants.DEBUG_TAG, "Error resetting photoWakeLock in PhotoTimerTask pre-photo", e);
                    }
                    photoWakeLock = null;
                    Utils.closeCamera();
                }

                photoWakeLock = powerManager!!.newWakeLock(
                        PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.FULL_WAKE_LOCK,
                        Constants.PHOTO_WAKE_LOCK);
                photoWakeLock?.acquire();

                val exposures = Constants.DEFAULT_EXPOSURES.map { exposure ->
                    Pair(File(storageRoot, runPrefix+"_"+String.format("%04d",Status.photoTimerAttemptCount)+"_"+(exposure+50).toString()+"_"+Constants.PHOTO_FILE), exposure);
                }
                Thread.sleep(1000);
                Utils.openCamera();
                Utils.takePhotoBurst(applicationContext, getLastKnownLocation(), exposures, { isSuccess ->
                    try {
                        photoWakeLock?.release();
                    } catch (e: Exception) {
                        Log.e(Constants.DEBUG_TAG, "Error resetting photoWakeLock in PhotoTimerTask post-photo", e);
                    }
                    photoWakeLock = null;
                    if(isSuccess && exposures.map({ p -> p.first.exists()}).reduce({a,b -> a && b})) {
                        Status.photoTimerLastSuccess = Date();
                        Status.photoTimerSuccessCount = Status.photoTimerSuccessCount + 1;
                    }
                    if(Config.photoUseSingleShot || !isSuccess) {
                        Utils.closeCamera();
                    }
                    val dpmReceiver = ComponentName(applicationContext, ScreenLockReceiver::class.java);
                    val dpm = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager;
                    if(dpm.isAdminActive(dpmReceiver)) {
                        dpm.lockNow();
                    }

                });
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error taking photo", e);
            }
        }
    }
    @Volatile
    var photoTimer: Timer? = null


    override fun onBind(intent: Intent): IBinder {
        throw NotImplementedError();
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(Constants.DEBUG_TAG, "MainService onCreate")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // Initialize config
        Config.readFromPreferences(this);

        if (intent != null) {
            val action = intent.action

            when (action) {
                Constants.ACTION_START_FOREGROUND_SERVICE -> {
                    startForegroundService()
                }
                Constants.ACTION_STOP_FOREGROUND_SERVICE -> {
                    stopForegroundService()
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private fun startForegroundService() {
        val notification = NotificationCompat.Builder(applicationContext)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentText("CasaDeBalloon Tracker Running")
                    .build()
        startForeground(1, notification);
        startProcessing();
        Toast.makeText(applicationContext, "CasaDeBalloon Tracker Started", Toast.LENGTH_SHORT).show();
    }

    private fun stopForegroundService() {
        stopForeground(true);
        stopProcessing();
        Toast.makeText(applicationContext, "CasaDeBalloon Tracker Stopped", Toast.LENGTH_SHORT).show();
        stopSelf();
    }

    private fun startProcessing() {
        stopProcessing();

        // Reset status
        Status.lastLocation = null;
        Status.lastSensorEventMap.clear();
        Status.smsTimerLastAttempt =  null;
        Status.smsTimerLastSuccess = null;
        Status.smsTimerAttemptCount = 0;
        Status.smsTimerSuccessCount = 0;
        Status.trackerTimerLastAttempt = null;
        Status.trackerTimerLastSuccess = null;
        Status.trackerTimerAttemptCount = 0;
        Status.trackerTimerSuccessCount = 0;
        Status.photoTimerLastAttempt = null;
        Status.photoTimerLastSuccess = null;
        Status.photoTimerAttemptCount = 0;
        Status.photoTimerSuccessCount = 0;
        Status.sensorTimerLastAttempt = null;
        Status.sensorTimerLastSuccess = null;
        Status.sensorTimerAttemptCount = 0;
        Status.sensorTimerSuccessCount = 0;

        // Initialize power management
        powerManager = (getSystemService(Context.POWER_SERVICE) as PowerManager);
        wakeLock = powerManager!!.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK,
                Constants.SERVICE_WAKE_LOCK);
        wakeLock!!.acquire();
        if(photoWakeLock != null) {
            try {
                photoWakeLock?.release();
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error resetting photoWakeLock in startProcessing", e);
            }
            photoWakeLock = null;
        }

        // Initialize storage
        runPrefix = Config.name+"_"+SimpleDateFormat("yyyyMMdd_HHmmss").format(Date());
        storageRoot = Utils.getStorageRoot(applicationContext)!!;
        logFile = File(storageRoot, Constants.LOG_FILE);
        nmeaLogFile = File(storageRoot, runPrefix+"_"+Constants.NMEA_LOG_FILE);
        sensorLogFile = File(storageRoot, runPrefix+"_"+Constants.SENSOR_LOG_FILE);

        // Initialize location
        if(ContextCompat.checkSelfPermission(applicationContext, android.Manifest.permission.ACCESS_FINE_LOCATION).equals(PackageManager.PERMISSION_GRANTED)) {
            locationManager = (getSystemService(Context.LOCATION_SERVICE) as LocationManager);
            nmeaLocationListener = NMEALogListener();
            noopLocationListener = NoopLocationListener();
            locationManager!!.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    150,
                    0.0f,
                    noopLocationListener);
            locationManager!!.addNmeaListener(nmeaLocationListener);
        } else {
            Utils.logToFile(logFile, "Permission to ACCESS_FINE_LOCATION denied");
        }

        // Initialize sensors
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager;
        for (sensor in sensorManager!!.getSensorList(Sensor.TYPE_ALL)) {
            Utils.logToFile(logFile, "Identified sensor "+sensor.getName() + ":" + sensor.getType());
        }
        sensorEventListener = NoopSensorEventListener();
        pressureSensor = sensorManager!!.getDefaultSensor(Sensor.TYPE_PRESSURE);
        if (pressureSensor != null) {
            Utils.logToFile(logFile, "Using pressure sensor "+pressureSensor!!.getName());
            sensorManager!!.registerListener(sensorEventListener, pressureSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        temperatureSensor = sensorManager!!.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        if (temperatureSensor != null) {
            Utils.logToFile(logFile, "Using temperature sensor "+temperatureSensor!!.getName());
            sensorManager!!.registerListener(sensorEventListener, temperatureSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
        humiditySensor = sensorManager!!.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        if (humiditySensor != null) {
            Utils.logToFile(logFile, "Using humidity sensor "+humiditySensor!!.getName());
            sensorManager!!.registerListener(sensorEventListener, humiditySensor, SensorManager.SENSOR_DELAY_NORMAL);
        }

        // Initialize camera
        Utils.closeCamera();

        // Initialize timers
        smsTimer = Timer()
        smsTimer?.schedule(SMSTimerTask(), 15000L, Config.smsIntervalSeconds * 1000L)
        trackerTimer = Timer()
        trackerTimer?.schedule(TrackerTimerTask(), 20000L, Config.trackerIntervalSeconds * 1000L)
        sensorTimer = Timer()
        sensorTimer?.schedule(SensorTimerTask(), 25000L, Config.sensorIntervalSeconds * 1000L)
        photoTimer = Timer()
        photoTimer?.schedule(PhotoTimerTask(), 30000L, Config.photoIntervalSeconds * 1000L)

        Utils.logToFile(logFile, "CasaDeBalloon Tracker Started");
    }

    private fun stopProcessing() {

        // Cleanup timers
        if(smsTimer != null) {
            smsTimer?.cancel();
            smsTimer?.purge();
        }
        if(trackerTimer != null) {
            trackerTimer?.cancel();
            trackerTimer?.purge();
        }
        if(sensorTimer != null) {
            sensorTimer?.cancel();
            sensorTimer?.purge();
        }
        if(photoTimer != null) {
            photoTimer?.cancel();
            photoTimer?.purge();
        }
        smsTimer = null;
        trackerTimer = null;
        sensorTimer = null;
        photoTimer = null;

        // Cleanup camera
        Utils.closeCamera();

        // Cleanup sensors
        if(sensorManager != null && sensorEventListener != null && pressureSensor != null) {
            sensorManager!!.unregisterListener(sensorEventListener, pressureSensor);
        }
        if(sensorManager != null && sensorEventListener != null && temperatureSensor != null) {
            sensorManager!!.unregisterListener(sensorEventListener, temperatureSensor);
        }
        if(sensorManager != null && sensorEventListener != null && humiditySensor != null) {
            sensorManager!!.unregisterListener(sensorEventListener, humiditySensor);
        }
        pressureSensor = null;
        temperatureSensor = null;
        humiditySensor = null;
        sensorEventListener = null;
        sensorManager = null;

        // Cleanup location
        if(locationManager != null && nmeaLocationListener != null) {
            locationManager?.removeNmeaListener(nmeaLocationListener);
        }
        if(locationManager != null && noopLocationListener != null) {
            locationManager?.removeUpdates(noopLocationListener);
        }
        noopLocationListener = null;
        nmeaLocationListener = null;
        locationManager = null;

        // Cleanup storage
        logFile = null;
        nmeaLogFile = null;
        sensorLogFile = null;
        runPrefix = null;
        storageRoot = null;

        // Cleanup power management
        if(wakeLock != null) {
            wakeLock?.release();
            wakeLock = null;
        }
        if(photoWakeLock != null) {
            try {
                photoWakeLock?.release();
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error resetting photoWakeLock in stopProcessing", e);
            }
            photoWakeLock = null;
        }
        powerManager = null;


        Log.d(Constants.DEBUG_TAG, "CasaDeBalloon Tracker Stopped")
    }

    fun getLastKnownLocation(): Location? {
        if(ContextCompat.checkSelfPermission(applicationContext, android.Manifest.permission.ACCESS_FINE_LOCATION).equals(PackageManager.PERMISSION_GRANTED)) {
            return locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } else {
            return null;
        }
    }
}